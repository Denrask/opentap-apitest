﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiTester.TestResponseExtension
{
    /// <summary>
    /// Extend <see cref="RequestStep.ResponseTests"/> javascript engine with custom CLR type references. This allows the <see cref="RequestStep.ResponseTests"/> to use custom C# methods.
    /// </summary>
    public interface ITestResponseExtension
    {

    }
}
